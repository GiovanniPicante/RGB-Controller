#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>

const char* ssid = "DeepSpace_9";
const char* password = "U2.HhFinnley";
char wifiHostname[] = "sofaRGBController";

ESP8266WebServer server(80);

uint8_t LEDpinRed = 5;
uint8_t LEDpinGreen = 16;
uint8_t LEDpinBlue = 4;

uint16_t LEDvalueRed = 1023;
uint16_t LEDvalueGreen = 1023;
uint16_t LEDvalueBlue = 1023;

void setup() {  
  pinMode(LEDpinRed, OUTPUT);
  pinMode(LEDpinGreen, OUTPUT);
  pinMode(LEDpinBlue, OUTPUT);

  //connect to your local wi-fi network
  WiFi.begin(ssid, password);
  wifi_station_set_auto_connect(true);
  wifi_station_set_hostname(wifiHostname);
  
  server.on("/setcolor", handle_setcolor);
  server.onNotFound(handle_NotFound);

  server.begin();
}
void loop() {
  server.handleClient();
  analogWrite(LEDpinRed, LEDvalueRed);
  analogWrite(LEDpinGreen, LEDvalueGreen);
  analogWrite(LEDpinBlue, LEDvalueBlue);
}

void setColor(int valueRed, int valueGreen, int valueBlue){  
  LEDvalueRed = valueRed;
  LEDvalueGreen = valueGreen;
  LEDvalueBlue = valueBlue;
}

void handle_setcolor(){
  String valueRedString = server.arg("red");
  String valueGreenString = server.arg("green");
  String valueBlueString = server.arg("blue");

  int valueRed = atoi(valueRedString.c_str());
  int valueGreen = atoi(valueGreenString.c_str());
  int valueBlue = atoi(valueBlueString.c_str());
  
  setColor(valueRed, valueGreen, valueBlue);
  
  server.send(200);
}

void handle_NotFound(){
  server.send(404, "text/plain", "Not found");
}
