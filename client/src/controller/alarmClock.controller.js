ledApp.controller("alarmClockController", ["$scope", "$http", "$timeout", function($scope, $http, $timeout){
    $scope.houreRange = 6;
    $scope.minuteRange = 30;
    $scope.isActive = false;

    $scope.alarmDays = [false, false, false, false, false, false, false];

    function init(){
        $http({
            "method" : "GET",
            "url" : "/getAlarmClock"
        }).then(response => {
            let data = response.data;

            $scope.houreRange = data.time[0];
            $scope.minuteRange = data.time[1];
            $scope.isActive = data.isActive;

            for(var i = 0; i < data.days.length; i++){
                $scope.alarmDays[data.days[i] - 1] = true;
            }

            console.log($scope.alarmDays)
        });
    }

    function setAlarmClock(){
        let setAlarmDays = [];

        for(var i = 0; i < $scope.alarmDays.length; i++){
            if($scope.alarmDays[i]){
                setAlarmDays.push(i + 1);
            }
        }

        $http({
            "method" : "POST",
            "url" : "/setAlarmClock",
            "data" : { "time" : [$scope.houreRange, $scope.minuteRange], "days" : setAlarmDays }
        }).then(response => {
            console.log(response);
            
            init();
        });
    }

    function deactivateAlarmClock(){
        $http({
            "method" : "GET",
            "url" : "/deactivateAlarmClock"
        }).then(response => {
            console.log("deactivateAlarmClock");

            init();
        });
    }

    $scope.changeAlarmState = function(){
        if($scope.isActive){
            setAlarmClock();
        }
        else{
            deactivateAlarmClock();
        }
    }

    init();    
}]);