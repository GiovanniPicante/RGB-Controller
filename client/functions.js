var ledApp = angular.module('ledSteuerung', []);

ledApp.controller("sliderController", ["$scope", "$http", "$timeout", function($scope, $http, $timeout){
    $scope.stripId = "";
    $scope.cycle = [];
    $scope.stripIsOff = true;

    $scope.colorPreview = { "background-color" : "rgb(255, 255, 255)" };

    var getState = function(){
        return $http.get("/state?stripId=" + $scope.stripId, { "cache" : false });
    };

    $scope.toggleLight = function(){
        $http.get("/toggleLight?stripId=" + $scope.stripId, { "cache" : false }).then(function(){
            getState().then(function(response){
                var data = response.data;

                if(data.isOn == false){
                    $scope.stripIsOff = true;
                }
                else{
                    $scope.stripIsOff = false;
                }
            });            
        });
    };

    $scope.cyleChanged = function(color){
        var data = {
            stripId : $scope.stripId,
            color : color,
            cycle : $scope.cycle[color] * 5
        };

        $http.post("/updateCycle", data);
    };

    $scope.toggleRandomFade = function(){
        var data = {
            stripId : $scope.stripId
        };

        $http.post("/toggleRandomFade", data);
    };

    $timeout(function(){
        getState().then(function(response){
            var data = response.data;

            $scope.cycle["red"] = data.dcRed / 5;
            $scope.cycle["green"] = data.dcGreen / 5;
            $scope.cycle["blue"] = data.dcBlue / 5;

            if(data.isOn == false){
                $scope.stripIsOff = true;
            }
            else{
                $scope.stripIsOff = false;
            }
        });
    });
}]);