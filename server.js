//INCLUDES 
const express = require("express");
const bodyParser = require('body-parser');

var app = express();
app.use(bodyParser.json());

const rpio = require("rpio");
const pigpio = require("pigpio")
const Gpio = pigpio.Gpio;

const ledStrip = require('./server/stripsObj.js');
const remoteLedStrip = require("./server/remoteStripsObj.js");

const path = require('path');
app.use(express.static(path.join(__dirname, 'client')));
app.use(express.static(path.join(__dirname, 'node_modules')));

//I2C SETUP
rpio.i2cBegin();
rpio.i2cSetSlaveAddress(0x04);
rpio.i2cSetBaudRate(50000);

//ENVIRONMENT VARIABLES =========================================================================================================
var stripA, stripB;

stripA = new remoteLedStrip.strip("http://sofaRGBController");
stripA.init();

stripB = new ledStrip.strip({
    pnRed : 13,
    pnGreen : 5,
    pnBlue : 26
}, Gpio);

stripB.init();

//API ===========================================================================================================================
app.get("/toggleLight", function(request, response) {
    var stripId = request.query.stripId;
    var state = "ABC";

    switch(stripId){
        case "A":
            state = stripA.toggleOnOff();
            break;
        case "B":
            state = stripB.toggleOnOff();
            break;
    }
    
    response.send(state);
});

app.get("/state", function(request, response){
    var stripId = request.query.stripId;
    var returnObject = {};

    switch(stripId){
        case "A":
            returnObject = stripA.getJSON();
            break;
        case "B":
            returnObject = stripB.getJSON();
            break;
    }

    response.send(returnObject);
});

app.post("/updateCycle", function(request, response){
    var data = request.body;
    var stripId = data.stripId;
    var color = data.color;
    var cycleValue = data.cycle;

    switch(stripId){
        case "A":
            switch(color){
                case "red":
                    stripA.setColor("red", cycleValue);
                    break;
                case "green":
                    stripA.setColor("green", cycleValue);
                    break;
                case "blue":
                    stripA.setColor("blue", cycleValue);
                    break;
            }
            break;
        case "B":
            switch(color){
                case "red":
                    stripB.setColor("red", cycleValue);
                    break;
                case "green":
                    stripB.setColor("green", cycleValue);
                    break;
                case "blue":
                    stripB.setColor("blue", cycleValue);
                    break;
            }
            break;
    }

    response.send("colorSet");
});


app.post("/toggleRandomFade", function(request, response){  // !!!! HAS TO BE FIXED
    var data = request.body;
    var stripId = data.stripId;
    var responseText = "";

    switch(stripId){
        case "A":
            stripA.toggleRandomFade();
            responseText = "A: Fade";
            break;
        case "B":
            stripB.toggleRandomFade();
            responseText = "B: Fade";
            break;
    }

    response.send("colorSet");
});

app.get("/deactivateAlarmClock", function(request, response){
    stripA.deactivateAlarmClock();
    stripB.deactivateAlarmClock();

    response.send("Alarm is disabled");
});

app.post("/setAlarmClock", function(request, response){
    var data = request.body;
    var time = data.time;
    var days = data.days;

    stripA.setAlarmClock(time, days);
    stripB.setAlarmClock(time, days);

    response.send("Alarm is Set");
});

app.get("/getAlarmClock", function(request, response){
    response.send(stripA.getAlarmClock());
});


//STARTUP =======================================================================================================================
app.listen(80);
console.log("Server runs on port 80");

//SHUTDOWN ======================================================================================================================
process.stdin.resume();

var closeHandler = function(save){
    if(save){
        pigpio.terminate();
        console.log("\nAdee!");
    }
    
    process.exit();
};

// //do something when app is closing
// process.on('exit', closeHandler.bind(null, true));

// //catches ctrl+c event
// process.on('SIGINT', closeHandler.bind(null, false));