(function() {
    var request = require("request");
    var stripsObj = require("./stripsObj")


    var convert8BitTo10Bit = function(value8Bit){
        var percentage = Math.round((value8Bit / 255) * 100);
        var value10Bit = (1023 / 100) * percentage;

        return value10Bit;
    }

    var ledStrip = function(host){
        var newRemoteStrip = new stripsObj.strip();
        var that = newRemoteStrip;

        newRemoteStrip.writeColor = function(){
            var url = host + "/setcolor?red=" + convert8BitTo10Bit(that.dcRed) + "&green=" + convert8BitTo10Bit(that.dcGreen) + "&blue=" + convert8BitTo10Bit(that.dcBlue);
            request(url, function(err){
                if(err){
                    console.log(err);
                }
            });
        }

        newRemoteStrip.getJSON = function(){
            var returnJSON = {
                "dcRed" : this.dcRed,
                "dcGreen" : this.dcGreen,
                "dcBlue" : this.dcBlue,
                "isOn" : this.isOn
            };

            return returnJSON;
        };

        return newRemoteStrip;
    }

    module.exports.strip = ledStrip;
}());