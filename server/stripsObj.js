(function() {
    var effects = require('./effects.js');

    var ledStrip = function(config, Gpio, I2C){
        //Basic config ==========================================================================================================
        if(config){
            this.pnRed = config.pnRed;
            this.pnGreen = config.pnGreen;
            this.pnBlue = config.pnBlue;
        };

        config = !config ? {} : config;

        this.dcRed = config.dcRed !== undefined ? config.dcRed : 0;
        this.dcGreen = config.dcGreen !== undefined ? config.dcGreen : 0;
        this.dcBlue = config.dcBlue !== undefined ? config.dcBlue : 0;

        this.isOn = true;

        //GPIO 
        var gpioRed = Gpio ? new Gpio(this.pnRed, {mode: Gpio.OUTPUT}) : undefined;
        var gpioGreen = Gpio ? new Gpio(this.pnGreen, {mode: Gpio.OUTPUT}) : undefined;
        var gpioBlue = Gpio ? new Gpio(this.pnBlue, {mode: Gpio.OUTPUT}) : undefined;
        
        //I2C
        this.I2C = I2C;

        //TEMP VARS
        var lastRed = 0;
        var lastGreen = 0;
        var lastBlue = 0;

        //Methodes ==============================================================================================================
        //writeColor
        var writeColor = function(){
            gpioRed.pwmWrite(this.dcRed);
            gpioGreen.pwmWrite(this.dcGreen);
            gpioBlue.pwmWrite(this.dcBlue);
        }
        this.writeColor = writeColor;

        //SetColor
        var setColor = function(color, value){
            switch(color){
                case "red":
                    this.dcRed = value;
                    break;
                case "green":
                    this.dcGreen = value;
                    break;
                case "blue":
                    this.dcBlue = value;
                    break;
                case "all":
                    this.dcRed = value;
                    this.dcGreen = value;
                    this.dcBlue = value;
                    break;
                case "rgb" :
                    this.dcRed = value["r"];
                    this.dcGreen = value["g"];
                    this.dcBlue = value["b"];
                    break;        
            }

            this.writeColor();

            if(I2C !== undefined){
                var txbuf = new Buffer([this.dcRed, this.dcGreen, this.dcBlue]);
                I2C(txbuf);
            }

            if(this.dcRed == 0 && this.dcGreen == 0 && this.dcBlue == 0){
                isOn = false;
            }
        };
        this.setColor = setColor;

        //toggleOnOff
        var toggleOnOff = function(){
            if(this.isOn){
                lastRed = this.dcRed;
                lastGreen = this.dcGreen;
                lastBlue = this.dcBlue;

                this.setColor("all", 0);

                this.isOn = false;
                return {"state" : "off" };
            }
            else{
                this.setColor("rgb", {
                    "r" : lastRed,
                    "g" : lastGreen,
                    "b" : lastBlue
                });

                this.isOn = true;
                return {"state" : "on" };
            }
        }
        this.toggleOnOff = toggleOnOff;

        //toggleRandomFade
        var toggleRandomFade = function(){
            randomFade.toggleEffect();
        };
        this.toggleRandomFade = toggleRandomFade;

        //setAlarmClock
        var setAlarmClock = function(time, days){
            alarmClock.setAlarmClock(time, days);
        };
        this.setAlarmClock = setAlarmClock;

        //getAlarmClock
        var getAlarmClock = function(){
            return alarmClock.getAlarmClock();
        }
        this.getAlarmClock = getAlarmClock;

        //toggleAlarmClock
        var deactivateAlarmClock = function(){
            return alarmClock.deactivateAlarmClock();
        };
        this.deactivateAlarmClock = deactivateAlarmClock;

        //Functions =============================================================================================================
        this.init = function(){
            this.setColor("rgb", {
                "r" : this.dcRed,
                "g" : this.dcGreen,
                "b" : this.dcBlue
            });
        };

        this.getJSON = function(){
            var returnJSON = {
                "pnRed" : this.pnRed, 
                "pnGreen" : this.pnGreen, 
                "pnBlue" : this.pnBlue, 
                "dcRed" : this.dcRed,
                "dcGreen" : this.dcGreen,
                "dcBlue" : this.dcBlue,
                "isOn" : this.isOn
            };

            return returnJSON;
        }

        //Effects ===============================================================================================================
        var randomFade = new effects.randomFade({}, this.setColor); // !!!! HAS TO BE FIXED
        var alarmClock = new effects.alarmClock(this.setColor, { "time" : [6,30], "days" : [1,2,3,4,5] }); // !!!! HAS TO BE FIXED
    }

    module.exports.strip = ledStrip;
}());