 // !!!! HAS TO BE FIXED

(function () {
    var randomFade = function (config, setColorCallback) {
        //Config ================================================================================================================
        var minBrightness = config.minBrightness !== undefined ? config.minBrightness : 255;
        var maxBrightness = config.maxBrightness !== undefined ? config.maxBrightness : 765;
        var keepDuration = config.keepDuration !== undefined ? config.keepDuration : 1000;
        var effectFPS = config.effectFPS !== undefined ? config.effectFPS : 45;
        var effectDuration = config.effectDuration !== undefined ? config.effectDuration : 3000;

        //Enviroment Variables ==================================================================================================
        var fullIntervallDuration = effectDuration + keepDuration + 1000;

        var oRed = 0, oGreen = 0, oBlue = 0;

        var setColor = setColorCallback;

        var effectInterval;
        var ongoingEffect = null;

        //Functions =============================================================================================================
        /* Start / Abbort function */
        this.toggleEffect = function () {
            if (ongoingEffect == null) {
                ongoingEffect = setInterval(generateNewColor(), fullIntervallDuration); //Color change interval
            }
            else {
                clearInterval(effectInterval);
                clearInterval(ongoingEffect);
                ongoingEffect = null;
            }
        };
        
        /* Random Numer Generator */
        var generateRandomNumber = function (max, min) {
            return Math.floor(Math.random() * (max - min + 1) + min);
        }

        /* Random Color Generator */
        var generateNewColor = function () {
            var nRed, nGreen, nBlue;
            var brightness = 0;

            do {
                nRed = generateRandomNumber(0, 255);
                nGreen = generateRandomNumber(0, 255);
                nBlue = generateRandomNumber(0, 255);

                brightness = nRed + nGreen + nBlue;

            } while (brightness < minBrightness && brightness > maxBrightness);

            fadeToNewColor(nRed, nGreen, nBlue);
            return generateNewColor;
        }

        /* Effect Step Calculator */
        var calcStepWidth = function (oldValue, newValue, stepCount) {
            if (oldValue == newValue) {
                return 0;
            }
            if (oldValue < newValue) {
                return (newValue - oldValue) / stepCount;
            }
            if (oldValue > newValue) {
                return ((oldValue - newValue) / stepCount) * -1;
            }
        };

        /* Main Effect Function */
        var fadeToNewColor = function (nRed, nGreen, nBlue) {
            var stepDuration = Math.round(1000 / effectFPS);
            var stepCount = Math.round((effectFPS / 1000) * effectDuration);

            var redStepWidth = calcStepWidth(oRed, nRed, stepCount);
            var greenStepWidth = calcStepWidth(oGreen, nGreen, stepCount);
            var blueStepWidth = calcStepWidth(oBlue, nBlue, stepCount);

            var tempRed = oRed;
            var tempGreen = oGreen;
            var tempBlue = oBlue;

            var actualRed, actualGreen, actualBlue;

            var intervalCount = 0;

            effectInterval = setInterval(function () { //Fade Interval
                if (intervalCount == stepCount) {
                    clearInterval(effectInterval);
                }
                else {
                    intervalCount++;
                }

                tempRed = tempRed + redStepWidth;
                tempGreen = tempGreen + greenStepWidth;
                tempBlue = tempBlue + blueStepWidth;

                actualRed = Math.round(tempRed);
                if(actualRed <= 0) actualRed = 0;
                else if(actualRed >= 255) actualRed = 255;

                actualGreen = Math.round(tempGreen);
                if(actualGreen <= 0) actualGreen = 0;
                else if(actualGreen >= 255) actualGreen = 255;

                actualBlue = Math.round(tempBlue);
                if(actualBlue <= 0) actualBlue = 0;
                else if(actualBlue >= 255) actualBlue = 255;

                setColor("rgb", {"r" : actualRed, "g" : actualGreen, "b" : actualBlue});
            }, stepDuration);

            oRed = nRed;
            oGreen = nGreen;
            oBlue = nBlue;
        }
    };


    //Modul Export
    module.exports.effect = randomFade;
})();