(function () {
    var alarmClock = function (setColorCallback, config) {
        //Enviroment Variables ==================================================================================================
        var setColor = setColorCallback;
        var alarmHours = config.time[0];
        var alarmMinutes = config.time[1];
        var alarmDays = config.days; 
        var alarmActive = true;

        //Effect Variables ======================================================================================================
        var timingInterval = null;

        //Functions =============================================================================================================
        var alarmClockTimer = function(){
            var currentTime = new Date();
            var currentHours = currentTime.getHours();
            var curentMinutes = currentTime.getMinutes();
            var currentDay = currentTime.getDay();

            if(alarmDays.indexOf(currentDay) > -1 && currentHours == alarmHours && curentMinutes == alarmMinutes){
                setColor("rgb", {"r" : 255, "g" : 255, "b" : 255});
            }
        };
        
        this.deactivateAlarmClock = function(){
            alarmActive = false;
            clearInterval(timingInterval);
            timingInterval = null;
        };

        this.setAlarmClock = function(time, days){
            alarmHours = time[0];
            alarmMinutes = time[1];
            alarmDays = days;
            alarmActive = true;
            
            timingInterval = setInterval(function(){
                alarmClockTimer();
            }, 25000);
        };

        this.getAlarmClock = function(){
            return {
                "time" : [alarmHours, alarmMinutes],
                "days" : alarmDays,
                "isActive" : alarmActive
            };
        };
    };


    //Modul Export
    module.exports.alarm = alarmClock;
})();