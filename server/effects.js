(function(){
    var randomFade = require('./effectLibrary/randomFade.js');
    module.exports.randomFade = randomFade.effect;

    var alarmClock = require('./effectLibrary/alarmClock.js');
    module.exports.alarmClock = alarmClock.alarm;
})();